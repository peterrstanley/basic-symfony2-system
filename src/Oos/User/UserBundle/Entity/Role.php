<?php

namespace Oos\User\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Role
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Role
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="role_name", type="string", length=255)
     */
    private $roleName;

    /**
     * @var integer
     *
     * @ORM\Column(name="role_priority", type="integer")
     */
    private $rolePriority;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set roleName
     *
     * @param string $roleName
     * @return Role
     */
    public function setRoleName($roleName)
    {
        $this->roleName = $roleName;

        return $this;
    }

    /**
     * Get roleName
     *
     * @return string 
     */
    public function getRoleName()
    {
        return $this->roleName;
    }

    /**
     * Set rolePriority
     *
     * @param integer $rolePriority
     * @return Role
     */
    public function setRolePriority($rolePriority)
    {
        $this->rolePriority = $rolePriority;

        return $this;
    }

    /**
     * Get rolePriority
     *
     * @return integer 
     */
    public function getRolePriority()
    {
        return $this->rolePriority;
    }
}
