<?php

namespace Oos\User\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Oos\User\UserBundle\Entity\UserRole;
use Oos\User\UserBundle\Form\UserRoleType;

/**
 * UserRole controller.
 *
 * @Route("/userrole")
 */
class UserRoleController extends Controller
{

    /**
     * Lists all UserRole entities.
     *
     * @Route("/", name="/userroles")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('OosUserBundle:UserRole')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new UserRole entity.
     *
     * @Route("/", name="userrole_create")
     * @Method("POST")
     * @Template("OosUserBundle:UserRole:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new UserRole();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('userrole_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a UserRole entity.
    *
    * @param UserRole $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(UserRole $entity)
    {
        $form = $this->createForm(new UserRoleType(), $entity, array(
            'action' => $this->generateUrl('userrole_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new UserRole entity.
     *
     * @Route("/new", name="userrole_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new UserRole();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a UserRole entity.
     *
     * @Route("/{id}", name="userrole_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('OosUserBundle:UserRole')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserRole entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing UserRole entity.
     *
     * @Route("/{id}/edit", name="userrole_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('OosUserBundle:UserRole')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserRole entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a UserRole entity.
    *
    * @param UserRole $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(UserRole $entity)
    {
        $form = $this->createForm(new UserRoleType(), $entity, array(
            'action' => $this->generateUrl('userrole_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing UserRole entity.
     *
     * @Route("/{id}", name="userrole_update")
     * @Method("PUT")
     * @Template("OosUserBundle:UserRole:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('OosUserBundle:UserRole')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserRole entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('userrole_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a UserRole entity.
     *
     * @Route("/{id}", name="userrole_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('OosUserBundle:UserRole')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find UserRole entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('userrole'));
    }

    /**
     * Creates a form to delete a UserRole entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('userrole_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
